package com.endeal.emporium.item.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import com.endeal.emporium.option.api.Option;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
@JsonDeserialize
public class Attribute {
    private final String id;
    private final List<Option> options;
    private final int max;

    @JsonCreator
    public Attribute(String id, List<Option> options, int max) {
        this.id = id;
        this.options = options;
        this.max = max;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        return this == another || another instanceof Attribute && equalTo((Attribute) another);
    }

    private boolean equalTo(Attribute another) {
        return this.id.equals(another.id) && this.options.equals(another.options) && this.max == another.max;
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + id.hashCode();
        h = h * 17 + options.hashCode();
        h = h * 17 + Integer.hashCode(max);
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Option").add("id", this.id).add("options", this.options).add("max", this.max).toString();
    }
}
