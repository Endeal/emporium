package com.endeal.emporium.item.api;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public class AttributeService extends Service {

    ServiceCall<Item, Done> createItem();

    @Override
    public Descriptor descriptor() {
        // @formatter:off
        return Service.named("attributeservice").withCalls(
                Service.namedCall("/attributes",  this::createAttribute),
                Service.pathCall("/attributes/:id", this::streamAttributes),
                Service.namedCall("/patrons/reset", this::resetPassword),
                Service.pathCall("/patrons/vendor/:id", this::addVendor),
                Service.pathCall("/patrons/item/:id", this::addItem),
                Service.pathCall("/patrons/:id", this::getPatron),
                Service.namedCall("/patrons/stream", this::stream)
        ).withAutoAcl(true);
        // @formatter:on
    }
}