package com.endeal.emporium.patron.api;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public interface PatronService extends Service {
    ServiceCall<Patron, Done> createPatron();

    ServiceCall<Credential, String> login();

    ServiceCall<String, Done> resetPassword();

    ServiceCall<NotUsed, Done> addVendor(String vendorId);

    ServiceCall<NotUsed, Done> addItem(String itemId);

    ServiceCall<NotUsed, Patron> getPatron(String patronId);

    ServiceCall<Source<LivePatronRequest, NotUsed>, Source<Patron, NotUsed>> stream();

    @Override
    default Descriptor descriptor() {
        // @formatter:off
        return Service.named("patronservice").withCalls(
                Service.namedCall("/patrons",  this::createPatron),
                Service.namedCall("/patrons/login", this::login),
                Service.namedCall("/patrons/reset", this::resetPassword),
                Service.pathCall("/patrons/vendor/:id", this::addVendor),
                Service.pathCall("/patrons/item/:id", this::addItem),
                Service.pathCall("/patrons/:id", this::getPatron),
                Service.namedCall("/patrons/stream", this::stream)
        ).withAutoAcl(true);
        // @formatter:on
    }
}
