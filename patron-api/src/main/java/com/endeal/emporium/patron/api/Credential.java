package com.endeal.emporium.patron.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public class Credential {
    private final String identifier;
    private final String verifier;

    @JsonCreator
    public Credential(String identifier, String verifier) {
        this.identifier = identifier;
        this.verifier = verifier;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getVerifier() {
        return this.verifier;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Credential && equalTo((Credential) another);
    }

    private boolean equalTo(Credential another) {
        return this.identifier.equals(another.identifier) && this.verifier.equals(another.verifier);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + identifier.hashCode();
        h = h * 17 + verifier.hashCode();
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Identity").add("identifier", this.identifier).add("verifier", this.verifier).toString();
    }

}
