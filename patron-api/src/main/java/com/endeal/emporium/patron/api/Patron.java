package com.endeal.emporium.patron.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public class Patron {
    private final String id;
    private final Identity identity;
    private final String email;
    private final String password;

    @JsonCreator
    public Patron(String id, Identity identity, String email, String password) {
        this.id = id;
        this.identity = identity;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return this.id;
    }

    public Identity getIdentity() {
        return this.identity;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Patron && equalTo((Patron) another);
    }

    private boolean equalTo(Patron another) {
        return this.identity.equals(another.identity) && this.email.equals(another.email) && this.password.equals(another.password);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + identity.hashCode();
        h = h * 17 + email.hashCode();
        h = h * 17 + password.hashCode();
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Patron").add("identity", identity).add("email", email).toString();
    }
}
