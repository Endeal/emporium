package com.endeal.emporium.patron.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.ZonedDateTime;

@Immutable
@JsonDeserialize
public class Identity {
    private final String firstName, lastName;
    private final ZonedDateTime birthday;

    @JsonCreator
    public Identity(String firstName, String lastName, ZonedDateTime birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public ZonedDateTime getBirthday() {
        return this.birthday;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Identity && equalTo((Identity) another);
    }

    private boolean equalTo(Identity another) {
        return this.firstName.equals(another.firstName) && this.lastName.equals(another.lastName) && this.birthday.equals(another.birthday);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + firstName.hashCode();
        h = h * 17 + lastName.hashCode();
        h = h * 17 + birthday.hashCode();
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Identity").add("firstName", this.firstName).add("lastName", this.lastName).add("birthday", this.birthday).toString();
    }
}
