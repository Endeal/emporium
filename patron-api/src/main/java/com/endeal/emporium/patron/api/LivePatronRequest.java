package com.endeal.emporium.patron.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public final class LivePatronRequest {
    public final String patronId;

    @JsonCreator
    public LivePatronRequest(String patronId) {
        this.patronId = Preconditions.checkNotNull(patronId, "patronId");
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another) return true;
        return another instanceof LivePatronRequest
                && equalTo((LivePatronRequest) another);
    }

    private boolean equalTo(LivePatronRequest another) {
        return this.patronId.equals(another.patronId);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + this.patronId.hashCode();
        return h;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper("LivePatronRequest")
                .add("patronId", this.patronId)
                .toString();
    }
}
