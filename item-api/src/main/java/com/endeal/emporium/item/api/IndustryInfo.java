package com.endeal.emporium.item.api;

import com.endeal.emporium.item.api.info.Fitting;
import com.endeal.emporium.item.api.info.Nutrition;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public class IndustryInfo {
    private final Nutrition nutrition;
    private final Fitting fitting;

    @JsonCreator
    public IndustryInfo(Nutrition nutrition, Fitting fitting) {
        this.nutrition = nutrition;
        this.fitting = fitting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndustryInfo that = (IndustryInfo) o;
        return Objects.equal(nutrition, that.nutrition) &&
                Objects.equal(fitting, that.fitting);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(nutrition, fitting);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("nutrition", nutrition)
                .add("fitting", fitting)
                .toString();
    }
}
