package com.endeal.emporium.item.api;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public interface ItemService extends Service {

    ServiceCall<Item, Done> createItem();
    ServiceCall<NotUsed, Source<Item, NotUsed>> getItem(String id);
    ServiceCall<NotUsed, Source<Item, NotUsed>> getItems(String vendorId);

    @Override
    default Descriptor descriptor() {
        // @formatter:off
        return Service.named("itemservice").withCalls(
                Service.namedCall("/items",  this::createItem),
                Service.pathCall("/items/:id", this::getItem),
                Service.pathCall("/vendor/items", this::getItems)
        ).withAutoAcl(true);
        // @formatter:on
    }
}