package com.endeal.emporium.item.api;

import com.endeal.emporium.category.api.Category;
import com.endeal.emporium.item.api.Attribute;
import com.endeal.emporium.item.api.IndustryInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.endeal.emporium.option.api.Price;
import com.google.common.base.MoreObjects;

import java.util.List;

@Immutable
@JsonDeserialize
public class Item {
    private final String id;
    private final String name;
    private final String description;
    private final IndustryInfo industryInfo;
    private final Price price;
    private final List<Attribute> attributes;
    private final List<Category> categories;
    private final int points;

    @JsonCreator
    public Item(String id, String name, String description, IndustryInfo industryInfo, Price price,
                List<Attribute> attributes, List<Category> categories, int points) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.industryInfo = industryInfo;
        this.price = price;
        this.attributes = attributes;
        this.categories = categories;
        this.points = points;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        return this == another || another instanceof Item && equalTo((Item) another);
    }

    private boolean equalTo(Item another) {
        return this.id.equals(another.id) && this.name.equals(another.name) && this.description.equals(another.description) &&
                this.industryInfo.equals(another.industryInfo) && this.price.equals(another.price) &&
                this.attributes.equals(another.attributes) && this.categories.equals(another.categories) &&
                this.points == another.points;
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + this.id.hashCode();
        h = h * 17 + this.name.hashCode();
        h = h * 17 + this.description.hashCode();
        h = h * 17 + this.industryInfo.hashCode();
        h = h * 17 + this.price.hashCode();
        h = h * 17 + this.attributes.hashCode();
        h = h * 17 + this.categories.hashCode();
        h = h * 17 + Integer.hashCode(this.points);
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Item").add("id", this.id).add("name", this.name).add("description", this.description).
                add("industryInfo", this.industryInfo).add("price", this.price).add("attributes", this.attributes).
                add("categories", this.categories).add("points", this.points).toString();
    }
}
