package com.endeal.emporium.item.api.info;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
@JsonDeserialize
public class Nutrition {
    private final int calories;
    private final int fat;
    private final int sugar;
    private final int sodium;
    private final int carbs;
    private final List<String> ingredients;

    @JsonCreator
    public Nutrition(int calories, int fat, int sugar, int sodium, int carbs, List<String> ingredients) {
        this.calories = calories;
        this.fat = fat;
        this.sugar = sugar;
        this.sodium = sodium;
        this.carbs = carbs;
        this.ingredients = ingredients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nutrition nutrition = (Nutrition) o;
        return calories == nutrition.calories &&
                fat == nutrition.fat &&
                sugar == nutrition.sugar &&
                sodium == nutrition.sodium &&
                carbs == nutrition.carbs &&
                Objects.equal(ingredients, nutrition.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(calories, fat, sugar, sodium, carbs, ingredients);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("calories", calories)
                .add("fat", fat)
                .add("sugar", sugar)
                .add("sodium", sodium)
                .add("carbs", carbs)
                .add("ingredients", ingredients)
                .toString();
    }
}
