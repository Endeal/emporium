package com.endeal.emporium.item.api.info;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
@JsonDeserialize
public class Fitting {
    public enum Gender {
        MALE, FEMALE
    }

    private final Gender gender;
    private final double size;
    private final String scale;
    private final List<String> materials;

    @JsonCreator
    public Fitting(Gender gender, double size, String scale, List<String> materials) {
        this.gender = gender;
        this.size = size;
        this.scale = scale;
        this.materials = materials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fitting fitting = (Fitting) o;
        return Double.compare(fitting.size, size) == 0 &&
                gender == fitting.gender &&
                Objects.equal(scale, fitting.scale) &&
                Objects.equal(materials, fitting.materials);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(gender, size, scale, materials);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("gender", gender)
                .add("size", size)
                .add("scale", scale)
                .add("materials", materials)
                .toString();
    }
}
