package com.endeal.emporium.option.api;

import com.endeal.emporium.option.api.Price;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public class Option {
    private final String id;
    private final String name;
    private final Price price;
    private final int points;

    @JsonCreator
    public Option(String id, String name, Price price, int points) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.points = points;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Option && equalTo((Option) another);
    }

    private boolean equalTo(Option another) {
        return this.id.equals(another.id) && this.name.equals(another.name) && this.price.equals(another.price);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + this.id.hashCode();
        h = h * 17 + this.name.hashCode();
        h = h * 17 + this.price.hashCode();
        h = h * 17 + Integer.hashCode(this.points);
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Option").add("id", this.id).add("name", this.name).add("price", this.price).add("points", this.points).toString();
    }
}
