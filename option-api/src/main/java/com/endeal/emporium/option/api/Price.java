package com.endeal.emporium.option.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
@JsonDeserialize
public class Price {
    private final String currency;
    private final long value;

    @JsonCreator
    public Price(String currency, long value) {
        this.currency = currency;
        this.value = value;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Price && equalTo((Price) another);
    }

    private boolean equalTo(Price another) {
        return this.currency.equals(another.currency) && this.value == another.value;
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + currency.hashCode();
        h = h * 17 + Long.hashCode(this.value);
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Price").add("currency", this.currency).add("value", this.value).toString();
    }
}
