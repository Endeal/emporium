package com.endeal.emporium.item.api;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public class OptionService extends Service {

    ServiceCall<Item, Done> createItem();

    ServiceCall<Source<>, Option> getOption(String id);

    ServiceCall

    @Override
    public Descriptor descriptor() {
        // @formatter:off
        return Service.named("optionservice").withCalls(
                Service.namedCall("/options",  this::createOption),
                Service.pathCall("/options/:id", this::streamOption),
        ).withAutoAcl(true);
        // @formatter:on
    }
}