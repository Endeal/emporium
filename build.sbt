organization in ThisBuild := "com.endeal.emporium"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.8"

// Cassandra Settings
lagomCassandraEnabled in ThisBuild := false
lagomCassandraPort in ThisBuild := 9042

lazy val helloworldApi = project("helloworld-api")
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies += lagomJavadslApi
  )

lazy val helloworldImpl = project("helloworld-impl")
  .enablePlugins(LagomJava)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomJavadslPersistence,
      lagomJavadslTestKit
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(helloworldApi)

lazy val hellostreamApi = project("hellostream-api")
  .settings(version := "1.0-SNAPSHOT")
  .settings(
    libraryDependencies += lagomJavadslApi
  )

lazy val hellostreamImpl = project("hellostream-impl")
  .settings(version := "1.0-SNAPSHOT")
  .enablePlugins(LagomJava)
  .dependsOn(hellostreamApi, helloworldApi)
  .settings(
    libraryDependencies += lagomJavadslTestKit
  )

lazy val categoryApi = project("category-api")
  .settings(version := "1.0")
  .settings(libraryDependencies += lagomJavadslApi)

lazy val categoryImpl = project("category-impl")
  .settings(version := "1.0",
    libraryDependencies ++= Seq(
      lagomJavadslPersistence,
      lagomJavadslTestKit,
      lagomJavadslPubSub
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .enablePlugins(LagomJava)
  .dependsOn(categoryApi)

lazy val optionApi = project("option-api")
  .settings(version := "1.0")
  .settings(libraryDependencies += lagomJavadslApi)

lazy val attributeApi = project("attribute-api")
  .settings(version := "1.0")
  .settings(libraryDependencies += lagomJavadslApi)
  .dependsOn(optionApi)

lazy val itemApi = project("item-api")
  .settings(version := "1.0")
  .settings(libraryDependencies += lagomJavadslApi)
  .dependsOn(attributeApi, categoryApi)

lazy val patronApi = project("patron-api")
  .settings(version := "1.0")
  .settings(libraryDependencies += lagomJavadslApi)

lazy val patronImpl = project("patron-impl")
  .settings(version := "1.0",
    libraryDependencies ++= Seq(
      lagomJavadslPersistence,
      lagomJavadslTestKit
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .enablePlugins(LagomJava)
  .dependsOn(patronApi)

def project(id: String) = Project(id, base = file(id))
  .settings(eclipseSettings: _*)
  .settings(javacOptions in compile ++= Seq("-encoding", "UTF-8", "-source", "1.8", "-target", "1.8", "-Xlint:unchecked", "-Xlint:deprecation"))
  .settings(jacksonParameterNamesJavacSettings: _*) // applying it to every project even if not strictly needed.


// See https://github.com/FasterXML/jackson-module-parameter-names
lazy val jacksonParameterNamesJavacSettings = Seq(
  javacOptions in compile += "-parameters"
)

// Configuration of sbteclipse
// Needed for importing the project into Eclipse
lazy val eclipseSettings = Seq(
  EclipseKeys.projectFlavor := EclipseProjectFlavor.Java,
  EclipseKeys.withBundledScalaContainers := false,
  EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource,
  EclipseKeys.eclipseOutput := Some(".target"),
  EclipseKeys.withSource := true,
  EclipseKeys.withJavadoc := true,
  // avoid some scala specific source directories
  unmanagedSourceDirectories in Compile := Seq((javaSource in Compile).value),
  unmanagedSourceDirectories in Test := Seq((javaSource in Test).value)
)
