package com.endeal.emporium.category.impl;

import akka.Done;
import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.javadsl.Source;
import com.datastax.driver.core.Row;
import com.endeal.emporium.category.api.Category;
import com.endeal.emporium.category.api.CategoryService;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import com.lightbend.lagom.javadsl.pubsub.PubSubRegistry;

import play.Logger;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.completedFuture;

public class CategoryServiceImpl implements CategoryService {

    private final PersistentEntityRegistry persistentEntityRegistry;
    private final CassandraSession db;
    private final PubSubRegistry pubSubRegistry;
    private final Materializer materializer;
    private final Logger.ALogger log = Logger.of(CategoryServiceImpl.class);

    @Inject
    public CategoryServiceImpl(PersistentEntityRegistry persistentEntityRegistry,
                               CassandraSession db,
                               PubSubRegistry pubSubRegistry,
                               Materializer materializer) {
        this.persistentEntityRegistry = persistentEntityRegistry;
        this.db = db;
        this.pubSubRegistry = pubSubRegistry;
        this.materializer = materializer;
        persistentEntityRegistry.register(CategoryEntity.class);
        CompletionStage<Done> result = db.executeCreateTable(
                "CREATE TABLE IF NOT EXISTS categories ("
                        + "timestamp timestamp, categoryId timeuuid, name text, "
                        + "PRIMARY KEY (categoryId))");
        result.whenComplete((ok, err) -> {
            if (err != null) {
                this.log.error("Failed to create categories table, due to: " + err.getMessage(), err);
            }
        });
    }

    @Override
    public ServiceCall<Category, String> createCategory() {
        return category -> {
            UUID uuid = UUID.randomUUID();
            PersistentEntityRef<CategoryCommand> categoryRef = persistentEntityRegistry.refFor(CategoryEntity.class, uuid.toString());
            return db.executeWrite("INSERT INTO categories (timestamp, categoryId, name) VALUES (?, ?, ?)",
                            new Date(), uuid, category.getName()
                    ).thenApply(done -> uuid.toString());
        };
    }

    @Override
    public ServiceCall<Category, Done> updateCategory() {
        return request ->
                db.executeWrite("UPDATE categories SET name = ? WHERE categoryId = ?", request.getName(), )
                        .thenApply(done -> Done.getInstance());
    }

    @Override
    public ServiceCall<NotUsed, Category> getCategory(String id) {
        return response -> {
            CompletionStage<Optional<Row>> result =
                    db.selectOne("SELECT categoryId, timestamp, name FROM categories WHERE categoryId = ?", UUID.fromString(id));
            return result.thenApply(row -> {
                if (row.isPresent()) {
                    Row selection = row.get();
                    return new Category(UUID.fromString(id), selection.getString("name"));
                } else {
                    throw new NotFound("Failed to find category for ID: " + id);
                }
            });
        };
    }

    @Override
    public ServiceCall<NotUsed, Source<Category, ?>> streamCategory(String id) {
        return null;
    }
}