package com.endeal.emporium.category.impl;

import com.endeal.emporium.category.api.Category;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.lightbend.lagom.serialization.Jsonable;
import org.springframework.cglib.core.Local;

import javax.annotation.concurrent.Immutable;
import java.time.LocalDateTime;
import java.util.Optional;

public interface CategoryEvent extends Jsonable {

    @SuppressWarnings("serial")
    @Immutable
    @JsonDeserialize
    public class CategoryCreated implements CategoryEvent {
        private final Optional<Category> category;
        private final LocalDateTime timestamp;

        public CategoryCreated(Category category, LocalDateTime timestamp) {
            this.category = Optional.of(category);
            this.timestamp = timestamp;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CategoryCreated that = (CategoryCreated) o;
            return Objects.equal(this.category, that.category) &&
                    Objects.equal(this.timestamp, that.timestamp);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(this.category, this.timestamp);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("category", this.category)
                    .add("timestamp", this.timestamp)
                    .toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class NameChanged implements CategoryEvent {
        private final String name;

        @JsonCreator
        public NameChanged(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NameChanged that = (NameChanged) o;
            return Objects.equal(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(name);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", name)
                    .toString();
        }
    }
}
