package com.endeal.emporium.category.impl;

import com.endeal.emporium.category.api.Category;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.lightbend.lagom.serialization.CompressedJsonable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("serial")
@Immutable
@JsonDeserialize
public class CategoryState implements CompressedJsonable {
    private final Optional<Category> category;

    @JsonCreator
    public CategoryState(Optional<Category> category) {
        this.category = category;
    }

    public Optional<Category> getCategory() {
        return this.category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryState that = (CategoryState) o;
        return Objects.equal(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(category);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("category", category)
                .toString();
    }
}
