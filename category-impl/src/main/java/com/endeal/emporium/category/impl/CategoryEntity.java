package com.endeal.emporium.category.impl;

import akka.Done;
import com.endeal.emporium.category.api.Category;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

public class CategoryEntity extends PersistentEntity<CategoryCommand, CategoryEvent, CategoryState> {
    @Override
    public Behavior initialBehavior(Optional<CategoryState> snapshotState) {

        BehaviorBuilder b = newBehaviorBuilder(
                snapshotState.orElse(new CategoryState(Optional.empty()))
        );

        b.setCommandHandler(CategoryCommand.CreateCategory.class, (cmd, ctx) ->
                    ctx.thenPersist(new CategoryEvent.CategoryCreated(cmd.getCategory(), LocalDateTime.now()), evt ->
                        ctx.reply(entityId())));

        b.setCommandHandler(CategoryCommand.ChangeName.class, (cmd, ctx) ->
                ctx.thenPersist(new CategoryEvent.NameChanged(cmd.getName()),
                        evt -> ctx.reply(Done.getInstance())));

        b.setEventHandler(CategoryEvent.NameChanged.class,
                evt -> {
                    Optional<Category> oldCategory = state().getCategory();
                    final Category newCategory;

                    if (oldCategory.isPresent()) {
                        newCategory = new Category(evt.getName());
                    } else {
                        newCategory = new Category(evt.getName());
                    }

                    return new CategoryState(java.util.Optional.of(newCategory));
                }
        );

        b.setReadOnlyCommandHandler(CategoryCommand.GetCategory.class, (cmd, ctx) -> {
            ctx.reply(state().getCategory().get());
        });

        return b.build();
    }
}
