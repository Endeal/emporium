package com.endeal.emporium.category.impl;

import akka.Done;
import com.endeal.emporium.category.api.Category;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.CompressedJsonable;
import com.lightbend.lagom.serialization.Jsonable;
import javafx.scene.control.TextFormatter;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

public interface CategoryCommand extends Jsonable {

    @Immutable
    @JsonDeserialize
    public final class CreateCategory implements CategoryCommand, CompressedJsonable, PersistentEntity.ReplyType<String> {
        private final Category category;

        public CreateCategory(Category category) {
            this.category = category;
        }

        public Category getCategory() {
            return this.category;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CreateCategory createCategory = (CreateCategory) o;
            return Objects.equal(this.category, createCategory.category);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(this.category);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("category", this.category)
                    .toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class ChangeName implements CategoryCommand, CompressedJsonable, PersistentEntity.ReplyType<Done> {
        private final String name;

        public ChangeName(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ChangeName that = (ChangeName) o;
            return Objects.equal(this.name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(this.name);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("name", this.name)
                    .toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class GetCategory implements CategoryCommand, CompressedJsonable, PersistentEntity.ReplyType<Category> {
        private final String categoryId;

        public GetCategory(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryId() {
            return this.categoryId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GetCategory getName = (GetCategory) o;
            return Objects.equal(this.categoryId, getName.categoryId);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(this.categoryId);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("categoryId", this.categoryId)
                    .toString();
        }
    }
}
