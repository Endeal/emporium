package com.endeal.emporium.category.impl;

import com.endeal.emporium.category.api.CategoryService;
import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

public class CategoryModule extends AbstractModule implements ServiceGuiceSupport {

    @Override
    protected void configure() {
        bindServices(serviceBinding(CategoryService.class, CategoryServiceImpl.class));
    }
}
