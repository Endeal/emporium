package com.endeal.emporium.category.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.UUID;

@Immutable
@JsonDeserialize
public class Category {
    private final UUID categoryId;
    private final String name;

    @JsonCreator
    public Category(UUID categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Category && equalTo((Category) another);
    }

    private boolean equalTo(Category another) {
        return this.name.equals(another.name);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + this.name.hashCode();

        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Category").add("name", this.name).toString();
    }
}
