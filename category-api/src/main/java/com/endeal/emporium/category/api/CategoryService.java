package com.endeal.emporium.category.api;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public interface CategoryService extends Service {

    ServiceCall<Category, String> createCategory();

    ServiceCall<NotUsed, Category> getCategory(String id);

    ServiceCall<Category, Done> updateCategory();

    ServiceCall<NotUsed, Source<Category, ?>> streamCategory(String id);

    @Override
    default Descriptor descriptor() {
        // @formatter:off
        return Service.named("categoryservice").withCalls(
                Service.namedCall("/categories", this::createCategory),
                Service.namedCall("/categories/update", this::updateCategory),
                Service.pathCall("/categories/:id", this::getCategory),
                Service.pathCall("/categories/stream/:id", this::streamCategory)
        ).withAutoAcl(true);
        // @formatter:on
    }
}