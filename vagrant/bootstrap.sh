#!/usr/bin/env bash

# Update package manager
apt-get update
apt-get upgrade

# Install GNU Screen
apt-get install -y screen

# Install Java 8
add-apt-repository ppa:webupd8team/java
apt-get update
apt-get install -y oracle-java8-installer

# Install unzip
apt-get install -y unzip

# Install Typesafe Activator
sh /vagrant/vagrant/activator.sh
wget https://downloads.typesafe.com/typesafe-activator/1.3.10/typesafe-activator-1.3.10.zip -O activator.zip
unzip activator.zip -d /opt/
rm activator.zip

# Add Activator to PATH
chown -R vagrant:vagrant /opt/activator-dist-1.3.10
touch /home/vagrant/.profile
echo -e "PATH=\$PATH:/opt/activator-dist-1.3.10/bin/\n" >> /home/vagrant/.profile

# Install Cassandra DB
wget http://downloads.datastax.com/datastax-ddc/datastax-ddc-3.7.0-bin.tar.gz -O cassandra.tgz
tar zxvf cassandra.tgz -C /opt
rm cassandra.tgz
chown -R vagrant:vagrant /opt/datastax-ddc-3.7.0/
echo -e "PATH=\$PATH:/opt/datastax-ddc-3.7.0/bin/\n" >> /home/vagrant/.profile
echo -e "CASSANDRA_HOME=\$PATH:/opt/datastax-ddc-3.7.0/bin/\n" >> /home/vagrant/.profile

# Download Redis
wget http://download.redis.io/releases/redis-3.2.3.tar.gz -O redis.tgz
tar zxvf redis.tgz -C /opt
rm redis.tgz
chown -R vagrant:vagrant /opt/redis-3.2.3/

# Install Redis
cd /opt/redis-3.2.3/
make
make install

# Install Node.js
apt-get install -y nodejs
echo -e "export SBT_OPTS=\"\${SBT_OPTS} -Dsbt.jse.engineType=Node -Dsbt.jse.command=\$(which node)\"\n" >> /home/vagrant/.profile

# Install IntelliJ
add-apt-repository ppa:mmk2410/intellij-idea-community
apt-get update
apt-get install -y intellij-idea-community

# Install IntelliJ SBT Plugin
wget http://plugins.jetbrains.com/files/1347/27110/scala-intellij-bin-2016.2.1.zip -O scala-plugin.zip
intellij-idea-community
unzip scala-plugin.zip -d /home/vagrant/.IdeaIC2016.2/config/plugins
rm scala-plugin.zip

# Shared folder setup
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
