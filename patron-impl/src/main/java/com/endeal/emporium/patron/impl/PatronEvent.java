package com.endeal.emporium.patron.impl;

import com.endeal.emporium.patron.api.Identity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

public interface PatronEvent extends Jsonable {
    @Immutable
    @JsonDeserialize
    public final class IdentityChanged implements PatronEvent {
        public final Identity identity;

        @JsonCreator
        public IdentityChanged(Identity identity) {
            this.identity = Preconditions.checkNotNull(identity, "identity");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another)
                return true;
            return another instanceof IdentityChanged && equalTo((IdentityChanged) another);
        }

        private boolean equalTo(IdentityChanged another) {
            return this.identity.equals(another.identity);
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.identity.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("IdentityChanged").add("identity", this.identity).toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class Created implements PatronEvent {
        public final String id;

        @JsonCreator
        public Created(String id) {
            this.id = Preconditions.checkNotNull(id, "identity");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another)
                return true;
            return another instanceof Created && equalTo((Created) another);
        }

        private boolean equalTo(Created another) {
            return this.id.equals(another.id);
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.id.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("Created").add("vendorId", this.id).toString();
        }
    }
}
