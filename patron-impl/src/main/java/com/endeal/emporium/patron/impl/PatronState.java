package com.endeal.emporium.patron.impl;

import com.endeal.emporium.patron.api.Identity;
import com.endeal.emporium.patron.api.Patron;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Optional;

@Immutable
@JsonDeserialize
public final class PatronState implements Jsonable {
    public final Optional<Patron> patron;

    @JsonCreator
    public PatronState(Optional<Patron> patron) {
        this.patron = Preconditions.checkNotNull(patron, "patron");
    }

    public PatronState changeEmail(String email) {
        if (!this.patron.isPresent()) {
            throw new IllegalStateException("Cannot change email before patron is created");
        }
        Patron originalPatron = this.patron.get();
        String originalId = originalPatron.getId();
        Identity originalIdentity = originalPatron.getIdentity();
        String originalPassword = originalPatron.getPassword();
        Patron patron = new Patron(originalId, originalIdentity, email, originalPassword);
        return new PatronState(Optional.of(patron));
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof PatronState && equalTo((PatronState) another);
    }

    private boolean equalTo(PatronState another) {
        return patron.equals(another.patron);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + patron.hashCode();
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("PatronState").add("patron", patron).toString();
    }
}
