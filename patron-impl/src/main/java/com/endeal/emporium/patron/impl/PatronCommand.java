package com.endeal.emporium.patron.impl;

import akka.Done;
import com.endeal.emporium.patron.api.Credential;
import com.endeal.emporium.patron.api.Patron;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

public interface PatronCommand extends Jsonable {
    @Immutable
    @JsonDeserialize
    public final class CreatePatron implements PatronCommand, PersistentEntity.ReplyType<Done> {
        public final Patron patron;

        @JsonCreator
        public CreatePatron(Patron patron) {
            this.patron = Preconditions.checkNotNull(patron, "patron");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another)
                return true;
            return another instanceof CreatePatron && equalTo((CreatePatron) another);
        }

        private boolean equalTo(CreatePatron another) {
            return this.patron.equals(another.patron);
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.patron.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("CreatePatron").add("patron", this.patron).toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class Login implements PatronCommand, PersistentEntity.ReplyType<String> {
        public final Credential credential;

        @JsonCreator
        public Login(Credential credential) {
            this.credential = Preconditions.checkNotNull(credential, "credential");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another)
                return true;
            return another instanceof Login && equalTo((Login) another);
        }

        private boolean equalTo(Login another) {
            return this.credential.equals(another.credential);
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.credential.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("Login").add("credential", this.credential).toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class GetPatron implements PatronCommand, PersistentEntity.ReplyType<Patron> {
        public final String id;

        @JsonCreator
        public GetPatron(String id) {
            this.id = Preconditions.checkNotNull(id, "vendorId");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another) {
                return true;
            }
            return another instanceof GetPatron;
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.id.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("GetPatron").add("vendorId", this.id).toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class ResetPassword implements PatronCommand, PersistentEntity.ReplyType<Done> {
        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another) {
                return true;
            }
            return another instanceof ResetPassword;
        }

        @Override
        public int hashCode() {
            return 15486571;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("ResetPassword").toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class AddVendor implements PatronCommand, PersistentEntity.ReplyType<Done> {
        public final String vendorId;

        @JsonCreator
        public AddVendor(String vendorId) {
            this.vendorId = Preconditions.checkNotNull(vendorId, "vendorId");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another) {
                return true;
            }
            return another instanceof AddVendor;
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.vendorId.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("AddVendor").add("vendorId", this.vendorId).toString();
        }
    }

    @Immutable
    @JsonDeserialize
    public final class AddItem implements PatronCommand, PersistentEntity.ReplyType<Done> {
        public final String itemId;

        @JsonCreator
        public AddItem(String itemId) {
            this.itemId = Preconditions.checkNotNull(itemId, "itemId");
        }

        @Override
        public boolean equals(@Nullable Object another) {
            if (this == another) {
                return true;
            }
            return another instanceof AddItem;
        }

        @Override
        public int hashCode() {
            int h = 31;
            h = h * 17 + this.itemId.hashCode();
            return h;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("AddItem").add("itemId", this.itemId).toString();
        }
    }
}
