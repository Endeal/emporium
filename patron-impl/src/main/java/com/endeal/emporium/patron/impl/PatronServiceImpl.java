package com.endeal.emporium.patron.impl;


import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.endeal.emporium.patron.api.Credential;
import com.endeal.emporium.patron.api.LivePatronRequest;
import com.endeal.emporium.patron.api.Patron;
import com.endeal.emporium.patron.api.PatronService;
import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

public class PatronServiceImpl implements PatronService {

    private final PersistentEntityRegistry persistentEntityRegistry;

    @Inject
    public PatronServiceImpl(PersistentEntityRegistry persistentEntityRegistry) {
        this.persistentEntityRegistry = persistentEntityRegistry;
        persistentEntityRegistry.register(PatronEntity.class);
    }

    @Override
    public ServiceCall<Patron, Done> createPatron() {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, request.getId());
            return ref.ask(new PatronCommand.CreatePatron(request));
        };
    }

    @Override
    public ServiceCall<Credential, String> login() {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, request.getIdentifier());
            return ref.ask(new PatronCommand.Login(request));
        };
    }

    @Override
    public ServiceCall<String, Done> resetPassword() {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, request);
            return ref.ask(new PatronCommand.ResetPassword());
        };
    }

    @Override
    public ServiceCall<NotUsed, Patron> getPatron(String patronId) {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, patronId);
            return ref.ask(new PatronCommand.GetPatron(patronId));
        };
    }

    @Override
    public ServiceCall<NotUsed, Done> addVendor(String vendorId) {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, vendorId);
            return ref.ask(new PatronCommand.AddVendor(vendorId));
        };
    }

    @Override
    public ServiceCall<NotUsed, Done> addItem(String itemId) {
        return request -> {
            PersistentEntityRef<PatronCommand> ref = persistentEntityRegistry.refFor(PatronEntity.class, itemId);
            return ref.ask(new PatronCommand.AddItem(itemId));
        };
    }

    @Override
    public ServiceCall<Source<LivePatronRequest, NotUsed>, Source<Patron, NotUsed>> stream() {
        return null;
    }

    @Override
    public Descriptor descriptor() {
        return null;
    }
}
