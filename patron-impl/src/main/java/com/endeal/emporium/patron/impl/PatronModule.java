package com.endeal.emporium.patron.impl;

import com.endeal.emporium.patron.api.PatronService;
import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

public class PatronModule extends AbstractModule implements ServiceGuiceSupport {
    @Override
    protected void configure() {
        bindServices(serviceBinding(PatronService.class, PatronServiceImpl.class));
    }
}