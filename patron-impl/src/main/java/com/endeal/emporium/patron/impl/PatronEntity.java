package com.endeal.emporium.patron.impl;

import akka.Done;
import com.endeal.emporium.patron.api.Patron;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import java.util.Optional;


public class PatronEntity extends PersistentEntity<PatronCommand, PatronEvent, PatronState> {
    @Override
    public Behavior initialBehavior(Optional<PatronState> snapshotState) {
        BehaviorBuilder builder = newBehaviorBuilder(
                snapshotState.orElse(
                    new PatronState(Optional.empty())
                )
        );
        builder.setCommandHandler(PatronCommand.CreatePatron.class, (command, context) ->
                context.thenPersist(new PatronEvent.Created(command.patron.getId()),
                        evt -> context.reply(Done.getInstance())));

        builder.setReadOnlyCommandHandler(PatronCommand.Login.class, (command, context) ->
                context.reply(state().patron.get().getId())
        );

        builder.setEventHandler(PatronEvent.IdentityChanged.class,
                evt -> new PatronState(Optional.of(new Patron(state().patron.get().getId(), evt.identity,
                        state().patron.get().getEmail(), state().patron.get().getPassword()))));

        builder.setReadOnlyCommandHandler(PatronCommand.GetPatron.class, (command, context) ->
                context.reply(state().patron.get())
        );

        return builder.build();
    }
}
